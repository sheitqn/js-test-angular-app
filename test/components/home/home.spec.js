(function() {
  'use strict';

  describe('Component: home', function () {

    var controller, compile, rootScope, ms;

    beforeEach(module('movpa.components.home'));
    beforeEach(module('movpa.service.movies'));
    beforeEach(module('templates'));

    beforeEach(inject(function($rootScope, $compile, $componentController, _MoviesService_) {
      controller = $componentController;
      compile = $compile;
      rootScope = $rootScope;
      ms = _MoviesService_;
    }));

    describe('Home Controller', function() {
      it('Test Case', function () {
        var $scope = {};

        // Controller
        var ctrl = controller('home');
        expect(ctrl.name).toEqual('Home');

        // $onInit
        ctrl.$onInit();
        expect(ctrl.onInit).toEqual('Success');
      });
    });

    // selector
    describe('Home templateUrl', function() {
      it('Test Case', function () {
        var factory = compile('<home></home>');
        var scope = rootScope.$new();
        var element = factory(scope);
        scope.$digest();
        console.log(element);
      });
    });
  });
})();
