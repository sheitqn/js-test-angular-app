(function() {
  'use strict';

  describe('Component: movie', function () {

    var controller, compile, rootScope;

    beforeEach(module('movpa.components.movie'));
    beforeEach(module('templates'));

    beforeEach(inject(function($rootScope, $compile, $componentController) {
      controller = $componentController;
      compile = $compile;
      rootScope = $rootScope;
    }));

    describe('Movie Controller', function() {
      it('Test Case', function () {
        var $scope = {};

        // Controllerの生成
        var ctrl = controller('movie', {$scope: $scope});
        expect(ctrl.name).toEqual('Movie');

        // $onInitの実行
        ctrl.$onInit();
        expect(ctrl.onInit).toEqual('Success');
      });
    });

    // selector
    describe('Contact templateUrl', function() {
      it('Test Case', function () {
        var factory = compile('<movie></movie>');
        var scope = rootScope.$new();
        var element = factory(scope);
        scope.$digest();
        console.log(element);
      });
    });
  });
})();
