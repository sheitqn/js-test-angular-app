describe('protractor sample', function() {
  'use strict';

  beforeEach(function () {
    browser.get('http://localhost:9001');
  });

  it('home test', function() {
    var home = element(by.css('a[ng-link="[\'Home\']"]'));
    home.click();

    var title = element(by.css('footer'));
    expect(title.getText()).toEqual('By Habib');
  });
});
