/**
 * MoviesService Service module.
 *
 * @module movpa.service.movies
 */
(function () {
  'use strict';

  angular
    .module('movpa.service.movies', [
      'ngResource'
    ])
    .factory('MoviesService', MoviesService);

  MoviesService.$inject = ['$resource'];

  /**
   * @class MoviesService
   * @constructor
   */
  function MoviesService($resource) {
    return $resource('/api/movies');
  }
})();
