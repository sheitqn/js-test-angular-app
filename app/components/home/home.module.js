/**
 * Home Components module.
 *
 * @module movpa.components.home
 */
(function () {
  'use strict';

  angular
    .module('movpa.components.home', [
      'movpa.service.movies'
    ]);
})();
