/**
 * Home Components module.
 *
 * @module movpa.components.home
 */
(function () {
  'use strict';

  angular
    .module('movpa.components.home')
    .component('home', {
      controller: Controller,
      templateUrl: 'components/home/home.html',
      $canActivate: $canActivate
    });

  Controller.$inject = ['MoviesService'];

  /**
   * Controller
   *
   * @class Controller
   * @constructor
   */
  function Controller(MoviesService) {
    var ctrl = this;
    ctrl.name = 'Home';
    ctrl.MoviesService = MoviesService;
  }

  function $canActivate() {
    return true;
  }

  Controller.prototype.$onInit = function() {
    var ctrl = this;
    ctrl.onInit = 'Success';
    var moviesCat = this.MoviesService.query().$promise;
    moviesCat
      .then(function (list) {
        ctrl.list = list;
      })
      .catch(function (e) {
        ctrl.e = e;
      });
  };
})();
