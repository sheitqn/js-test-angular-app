/**
 * About Components module.
 *
 * @module movpa.components.movie
 */
(function () {
  'use strict';

  angular
    .module('movpa.components.movie', ['ng.deviceDetector']);
})();
