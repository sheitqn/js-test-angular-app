(function () {
  'use strict';

  angular
    .module('movpa.components.movie')
    .component('movie', {
      controller: Controller,
      templateUrl: function(deviceDetector) {
        return (deviceDetector.isMobile()) ? 'components/movie/movie.mobile.html' : 'components/movie/movie.desktop.html';
      },
      $canActivate: $canActivate,
      bindings: {
        movies: '<',
        category: '<'
      }
    });

  Controller.$inject = ['deviceDetector'];

  /**
   * MovieController
   *
   * @class MovieController
   * @constructor
   */
  function Controller() {
    var ctrl = this;
    ctrl.name = 'Movie';
    /**
     * The controller activate makes it convenient to re-use the logic
     * for a refresh for the controller/View, keeps the logic together.
     *
     * @method $onInit
     */
    ctrl.$onInit = function() {
      ctrl.onInit = 'Success';
      ctrl.previousMovie = function() {
        var current = document.getElementById(ctrl.category).getElementsByClassName('current')[0];
        var index = parseInt(current.id.split('_').pop(), 10);
        var indexP = (((index - 1) % ctrl.movies.length) + ctrl.movies.length) % ctrl.movies.length;
        var indexPp = (((index - 2) % ctrl.movies.length) + ctrl.movies.length) % ctrl.movies.length;
        var indexN = (((index + 1) % ctrl.movies.length) + ctrl.movies.length) % ctrl.movies.length;
        var indexNn = (((index + 2) % ctrl.movies.length) + ctrl.movies.length) % ctrl.movies.length;
        var indexNewp = (((index - 3) % ctrl.movies.length) + ctrl.movies.length) % ctrl.movies.length;
        var previous = document.getElementById(ctrl.category + '_' + indexP);
        var pprevious = document.getElementById(ctrl.category + '_' + indexPp);
        var next = document.getElementById(ctrl.category + '_' + indexN);
        var nnext = document.getElementById(ctrl.category + '_' + indexNn);
        $(current).removeClass('current').addClass('next');
        $(previous).removeClass('prev').addClass('current');
        $(pprevious).removeClass('pprev').addClass('prev');
        $(next).removeClass('next').addClass('nnext');
        $(nnext).removeClass('nnext').addClass('invisible');
        pprevious = document.getElementById(ctrl.category + '_' + indexNewp);
        $(pprevious).removeClass('invisible').addClass('pprev');
      };
      ctrl.nextMovie = function() {
        var current = document.getElementById(ctrl.category).getElementsByClassName('current')[0];
        var index = parseInt(current.id.split('_').pop(), 10);
        var indexP = mod(index - 1, ctrl.movies.length);
        var indexPp = mod(index - 2, ctrl.movies.length);
        var indexN = mod(index + 1, ctrl.movies.length);
        var indexNn = mod(index + 2, ctrl.movies.length);
        var indexNewn = mod(index + 3, ctrl.movies.length);
        var previous = document.getElementById(ctrl.category + '_' + indexP);
        var pprevious = document.getElementById(ctrl.category + '_' + indexPp);
        var next = document.getElementById(ctrl.category + '_' + indexN);
        var nnext = document.getElementById(ctrl.category + '_' + indexNn);
        $(current).removeClass('current').addClass('prev');
        $(previous).removeClass('prev').addClass('pprev');
        $(next).removeClass('next').addClass('current');
        $(nnext).removeClass('nnext').addClass('next');
        $(pprevious).removeClass('pprev').addClass('invisible');
        nnext = document.getElementById(ctrl.category + '_' + indexNewn);
        $(nnext).removeClass('invisible').addClass('nnext');
      };

      function mod(n, m) {
        return ((n % m) + m) % m;
      }
    };
  }

  /**
   * The controller canActivate makes it convenient to re-use the logic
   * for a refresh for the controller/View, keeps the logic together.
   *
   * @method canActivate
   */
  function $canActivate () {
    return true;
  }
})();
