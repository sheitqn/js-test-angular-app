/**
 * This is movpa module.
 *
 * @module movpa
 */
(function () {
  'use strict';

  angular
    .module('movpa')
    .controller('AppController', AppController);

  AppController.$inject = ['$rootRouter'];

  /**
  * AppController
  *
  * @class AppController
  * @main movpa
  * @constructor
  */
  function AppController ($rootRouter) {
    $rootRouter.config([
      {
        path: '/home',
        name:'Home',
        component: 'home',
        useAsDefault: true
      }
    ]);
  }
})();
