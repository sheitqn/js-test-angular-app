/**
 * This is movpa module.
 *
 * @module movpa
 */
(function () {
  'use strict';

  angular
    .module('movpa', [
      'ngComponentRouter',
      'ngScrollable',
      'ng.deviceDetector',
      'movpa.config',
      'movpa.components.home',
      'movpa.components.movie'
    ]);
})();
